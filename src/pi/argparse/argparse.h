#ifndef LAB8_ARGPARSE_H
#define LAB8_ARGPARSE_H

/**
 * Print usage information to [stderr].
 */
void printUsage();

/**
 * Get threads count from command line arguments.
 * @param argc first argument of function [main]
 * @param argv second argument of function [main]
 * @param p_result pointer to save parsed threads count to
 * @return [NO_ERROR] if threads count was stored in [*p_result] or non-[NO_ERROR] value if an error occurred
 */
int getThreadsCount(
        int argc,
        char **argv,
        unsigned long *p_result);

#endif //LAB8_ARGPARSE_H
