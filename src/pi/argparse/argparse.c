#include "argparse.h"

#include <stdio.h>
#include <errno.h>
#include <limits.h>
#include <stdlib.h>
#include <pthread.h>

#include "../../common/errhandle.h"

#ifndef PTHREAD_THREADS_MAX
#define PTHREAD_THREADS_MAX _POSIX_THREAD_THREADS_MAX
#endif

void printUsage() {
    fprintf(
            stderr,
            "Usage: pi {n_threads}\n"
            "    Computes pi using Leibniz's formula in parallel\n"
            "Arguments:\n"
            "    n_threads -> number of threads to run in parallel { Integer in range [1, %d] }\n",
            PTHREAD_THREADS_MAX
    );
}

int getThreadsCount(
        int argc,
        char **argv,
        unsigned long *p_result) {
    const int MIN_VALID_ARGC = 1;
    const int REQUIRED_ARGC = 2;

    if (argc < MIN_VALID_ARGC || NULL == argv) {
        errorfln("Invalid arguments vector (argv is %p, argc is %d)", argv, argc);
        return !NO_ERROR;
    }
    if (REQUIRED_ARGC != argc) {
        errorfln("Unexpected arguments count: expected %u, got %d", 1, argc - 1);
        return !NO_ERROR;
    }
    if (NULL == p_result) {
        errorfln("[%s] Unexpected pointer argument value: %p", __FUNCTION__, p_result);
        return !NO_ERROR;
    }

    char const *str_thread_num = argv[1];
    char *end_ptr;

    errno = NO_ERROR;
    long result = strtol(str_thread_num, &end_ptr, 10);
    if (NO_ERROR != errno || ((char) 0) != *end_ptr) {
        errorfln("\"%s\" is not a valid long integer representation", str_thread_num);
        return !NO_ERROR;
    }
    if (result <= 0 || result > PTHREAD_THREADS_MAX) {
        errorfln("Invalid threads count: expected in range [1, %d], got %ld", PTHREAD_THREADS_MAX, result);
        return !NO_ERROR;
    }

    *p_result = result;
    return NO_ERROR;
}
