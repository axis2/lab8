#ifndef LAB7_TIMESTAMP_H
#define LAB7_TIMESTAMP_H

#include <time.h>

#include "../errhandle.h"


long timespecToMs(struct timespec ts);

struct timespec msToTimespec(long timeMs);

int setTimeMs(clockid_t clock_id, long timeMs);

int getTimeMs(clockid_t clock_id, long *result);

int getResolutionMs(clockid_t clock_id, long *result);

int getResolutionTimespec(clockid_t clock_id, struct timespec *result);

#endif //LAB7_TIMESTAMP_H
