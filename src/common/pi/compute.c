#include "compute.h"


long double computePi(Range_t range) {
    long double partial_sum = 0.0;

    for (typeof(range.start) i = range.start; i < range.end; i += range.step) {
        partial_sum += 1.0 / ((long double) i * 4.0 + 1.0);
        partial_sum -= 1.0 / ((long double) i * 4.0 + 3.0);
    }

    return partial_sum;
}
