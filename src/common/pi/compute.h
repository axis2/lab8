#ifndef LAB8_COMPUTE_H
#define LAB8_COMPUTE_H

#include "../range.h"

#define NUM_STEPS       200000000u

long double computePi(Range_t range);

#endif //LAB8_COMPUTE_H
